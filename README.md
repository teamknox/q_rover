<div align="left">
<a href="/uploads/59169992827a64496cfce14e673287a7/IMG_3423.jpg"><img src="/uploads/59169992827a64496cfce14e673287a7/IMG_3423.jpg" width="252" height="179" alt="Crabs"></a>
<a href="/uploads/8234dd0b25f7dfd11ce7f3fc2efbbdb8/IMG_3346.jpg"><img src="/uploads/8234dd0b25f7dfd11ce7f3fc2efbbdb8/IMG_3346.jpg" width="252" height="179" alt="Crab"></a>
</div>

https://youtu.be/f2prV_57UrI

![BlockDiagram_wifi](/uploads/75ec2da2337124ca650299cf1cbce0d2/BlockDiagram_wifi.png)

# Overview
Q_rover is generic term for a mecanum-wheel rovers which are using Groovy-Quatro and Linux SBC.

# Motivation
Since Linux SBC performance is increasing, we can't stop to purchase the latest boards. However, controlling environment is very poor so far. Jupyter-lab gives us good envrironment not only software development but also remote control for various type of rovers/robot.

# Advantages
* Simple<br>
Jupyter-lab basis, integrated programming and controlling environment in one package. Each rover-controlling is mapped to browser's tab.

* Reusability<br>
**Write at once Run any Rover/Robot.** Your developed python script can run Groovy-Quatro based Rovers.


# Environment
You have to setup following environment to each rover. Software installation is totally depened distributed linux.

## Hardware
* Refer wiki in [English](https://gitlab.com/teamknox/groovy-quatro/-/wikis/Build-Guide:-Project-Crab-(Mecanum-Wheels-Rover)-in-English)/[Japanese](https://gitlab.com/teamknox/groovy-quatro/-/wikis/Build-Guide:-Project-Crab-(Mecanum-Wheels-Rover)-in-Japanese).

## Software
* jupyter-lab<br>
You have to install some modules to your SBC. You have to check import statement in your script (e.g. serial for UART, etc.).

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 
