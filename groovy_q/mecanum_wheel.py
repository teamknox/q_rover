gM = [0, 0, 0, 0]

def north(aSpeed):
    global gM
    gM[0] = gM[1] = aSpeed
    gM[2] = gM[3] = -aSpeed

def north_east(aSpeed):
    global gM
    gM[1] = gM[2] = 0
    gM[0] = aSpeed
    gM[3] = -aSpeed

def east(aSpeed):
    global gM
    gM[0] = gM[2] = aSpeed
    gM[1] = gM[3] = -aSpeed

def south_east(aSpeed):
    global gM
    gM[0] = gM[3] = 0
    gM[1] = -aSpeed
    gM[2] = aSpeed

def south(aSpeed):
    global gM
    gM[0] = gM[1] = -aSpeed
    gM[2] = gM[3] = aSpeed

def south_west(aSpeed):
    global gM
    gM[1] = gM[2] = 0
    gM[0] = -aSpeed
    gM[3] = aSpeed

def west(aSpeed):
    global gM
    gM[0] = gM[2] = -aSpeed
    gM[1] = gM[3] = aSpeed

def north_west(aSpeed):
    global gM
    gM[0] = gM[3] = 0
    gM[1] = aSpeed
    gM[2] = -aSpeed

def CW(aSpeed):
    global gM
    gM[0] = gM[1] = aSpeed
    gM[2] = gM[3] = aSpeed

def CCW(aSpeed):
    global gM
    gM[0] = gM[1] = -aSpeed
    gM[2] = gM[3] = -aSpeed

def stop():
    global gM
    gM[0] = gM[1] = gM[2] = gM[3] = 0

def drive(aSerialPort):
    global gM
    motorSpeed = ('s 4 %d %d %d %d\r\n' % (gM[0], gM[1], gM[2], gM[3]))
    aSerialPort.write(str.encode(motorSpeed))
    print (motorSpeed)
